import { createSlice } from '@reduxjs/toolkit';

export const appSlice = createSlice({
  name: 'app',
  initialState: {
    riskL: 10,
    defaultRiskL: 10,
    initialSum: 10000,
    fee: 0.01, 
    years: 10,
  },
  reducers: {
    setRiskL: (state, action) => {
      state.riskL = action.payload;
    },
    setInitialSum: (state, action) => {
      state.initialSum = action.payload;
    },
    setFee: (state, action) => {
      state.fee = action.payload;
    },
    setYears: (state, action) => {
      state.years = action.payload;
    },
  },
});

export const { setRiskL, setInitialSum, setFee, setYears } = appSlice.actions;

export default appSlice.reducer;
