import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from 'react-redux';
import {setRiskL} from './reducers/appSlicer';


const RiskLevelSelector = ({minRiskLevel = 3, maxRiskLevel = 25}) => {
    const riskL = useSelector(state => state.app.riskL);
    const [toggleAlert, setToggleAlert] = useState(false);
    const dispatch = useDispatch();
    const options = [];
    let defaultRiskL = useSelector(state => state.app.defaultRiskL);

    const onChange = (event) => {
        let riskLevel = parseInt(event.target.value);
        if (riskLevel < minRiskLevel){
            setToggleAlert(true);
        } else {
            setToggleAlert(false);
            dispatch(setRiskL(riskLevel));
        }
    }

    for(let k=1; k<=maxRiskLevel; ++k) {
        options.push(
            <option key={k} data-testid={k} value={k}>{k}</option>
        );
    }

    return (
        <div>
            Risk level:
            <select onChange={onChange} defaultValue={defaultRiskL} data-testid="riskL-select-option">
                {options}
            </select>
            {
                toggleAlert ? <p data-testid="alert-msg" style={{color: `red` }}>{`Sorry! Min Value is ${minRiskLevel}`}</p> : null
            }
            <p data-testid="risk-level">{`Risk Level: ${riskL}`}</p>
            
            
        </div>
    )
}

RiskLevelSelector.propTypes = {
    minRiskLevel: PropTypes.number, 
    maxRiskLevel: PropTypes.number, 
}


export default RiskLevelSelector;
