import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { setInitialSum, setFee, setYears} from './reducers/appSlicer';


const ControlPanel = () => {
    const dispatch = useDispatch();
    let initialSum = useSelector(state => state.app.initialSum);
    let fee = useSelector(state => state.app.fee);
    let years = useSelector(state => state.app.years);

    const onChange = (event) => {
        if(event.target.value !== ''){
          let tmpInitSum = parseInt(event.target.value);
          dispatch(setInitialSum(tmpInitSum));
        }
    }

    const onPlanChange = (event) => {
      dispatch(setFee(event.target.value));
    }

    const onYearsChange = (event) => {
      dispatch(setYears(event.target.value));
    }


    let plans = [{name: 'Plan A', fee: 0.01}, {name: 'Plan B', fee: 0.05}, {name: 'Plan C', fee: 1.0}]
    let options = plans.map((val,index) => {
      return (
        <option key={val.name} data-testid={`plan-${index}`} value={val.fee}>{val.name}</option>
      )
    })


    return (
        <div>
          <div>
          Initial Sum:
            <input type="number" onChange={onChange} data-testid="initial-sum-type"></input>

            <p data-testid="initial-sum">{`Initial Sum: ${initialSum}`}</p>
          </div>
          <div>
          Plan:
          <select onChange={onPlanChange} data-testid="plan-select-option">
                {options}
            </select>

            <p data-testid="plan-type">{`Plan fee: ${fee}`}</p>
          </div>
          <div>
          Years:
            <input type="number" onChange={onYearsChange} data-testid="years-type"></input>

            <p data-testid="years">{`Years: ${years}`}</p>
          </div>
        </div>
    )
}



export default ControlPanel;
