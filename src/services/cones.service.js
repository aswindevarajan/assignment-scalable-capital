import axios from 'axios';


export const getCones = async () => {
  const response = await axios.get('/api/cones');
  return response.data;
}