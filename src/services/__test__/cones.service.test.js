import mockAxios from "axios";
import cones from '../../../cones.json';
import {getCones} from '../cones.service';


it('should get the cones', async () => {
  mockAxios.get.mockImplementationOnce(() => {
    return Promise.resolve({
      data: cones
    })
  })

  const data = await getCones();
  expect(data).toBe(cones);
  expect(mockAxios.get).toHaveBeenCalledTimes(1);
})