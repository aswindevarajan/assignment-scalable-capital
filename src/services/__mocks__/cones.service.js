import cones from '../../../cones.json';

export const getCones = async () => {
  const response = await new Promise((resolve) => {
    resolve({data: cones})
  })
  return response.data;
}