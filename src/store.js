import { configureStore } from '@reduxjs/toolkit';
import appReducer from './reducers/appSlicer.js';

export default configureStore({
  reducer: {
    app: appReducer,
  },
});
