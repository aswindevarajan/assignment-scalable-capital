import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Menu from "./menu";
import RiskLevelSelector from "./risk-level-selector";
import Table from "./table";
import Chart from "./chart";
import ControlPanel from "./control-panel";

const MainPage = () => {
  return (
    <Router>
      <div>
        <Menu />
        <ControlPanel></ControlPanel>
        <RiskLevelSelector></RiskLevelSelector>
        <Route exact path="/" component={() => <Table />} />
        <Route path="/table" component={() => <Table />} />
        <Route path="/chart" component={() => <Chart />} />
      </div>
    </Router>
  );
};

export default MainPage;
