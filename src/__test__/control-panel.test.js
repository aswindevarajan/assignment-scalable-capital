import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { render, cleanup, waitFor, act, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/jest-dom";
import ControlPanel from "../control-panel";

describe("Testing the control panel", () => {
  afterEach(cleanup);

  const initState = {
    app: {
      riskL: 10,
      defaultRiskL: 10,
      initialSum: 10000,
    },
  };

  function reducer(state = initState, action) {
    switch (action.type) {
      case "setRiskL":
        return {
          ...state,
          app: Object.assign({}, state.app, { riskL: action.payload }),
        };
      case "setInitialSum":
        return {
          ...state,
          app: Object.assign({}, state.app, { initialSum: action.payload }),
        };
      default:
        return state;
    }
  }

  function renderWithRedux(
    component,
    { initialState, store = createStore(reducer, initialState) } = {}
  ) {
    return {
      ...render(<Provider store={store}>{component}</Provider>),
    };
  }

  it("renders with default riskL", () => {
    renderWithRedux(<ControlPanel />);
    expect(screen.getByTestId("initial-sum").innerHTML).toContain(
      `Initial Sum: 10000`
    );
  });

  it("should show the new updated initial sum", async () => {
    renderWithRedux(<ControlPanel />);
    act(() => {
      await userEvent.type(screen.getByTestId("initial-sum-type"), "100");
    });

    // await waitFor(() => screen.getByTestId("initial-sum").innerHTML).toBe(
    //   "Initial Sum: 100"
    // );
    await expect(screen.findByTestId('initial-sum')).toBe('Initial Sum: 100');
  });
});
