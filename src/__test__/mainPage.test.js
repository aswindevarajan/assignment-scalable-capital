import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { render, cleanup, waitFor, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import App from '../app';
import cones from '../../cones';
import axiosMock from 'axios';

jest.mock('../services/cones.service')


describe('App testing', () => {
  afterEach(cleanup);

  const initState = {
    app: {
      riskL: 10,
      defaultRiskL: 10,
    },
  };

  function reducer(state = initState, action) {
    switch (action.type) {
      case 'setRiskL':
        return {
          ...state,
          app: Object.assign({}, state.app, { riskL: action.payload }),
        };
      default:
        return state;
    }
  }

  function renderWithRedux(
    component,
    { initialState, store = createStore(reducer, initialState) } = {}
  ) {
    return {
      ...render(<Provider store={store}>{component}</Provider>),
    };
  }


  it('should prompt user to change the risk level ', () => {
    renderWithRedux(<App />);
    // update the value of risk level
    expect(screen.getByTestId('prompt-user').innerHTML).toBe(
      'Please Select your risk level...'
    );
  });

  it('should update the table data on change of risk level', async () => {
    axiosMock.get.mockResolvedValueOnce({data: cones});
    renderWithRedux(<App />);
    await userEvent.selectOptions(screen.getByTestId('riskL-select-option'), ['4']);
    await waitFor(() => expect(screen.getByTestId('history-table')).toBeInTheDocument());
  });

});
