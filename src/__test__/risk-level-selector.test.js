import React from 'react'; 
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import { render, cleanup, waitFor, fireEvent,  screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom';
import RiskLevelSelector from '../risk-level-selector';
import cones from '../../cones.json';
import axiosMock from 'axios';





describe('risk-level-selector', () => {
  afterEach(cleanup);

  const initState = {
    app: {
      riskL: 10,
      defaultRiskL: 10,
    }
  }
  
  function reducer(state = initState, action){
    switch(action.type){
      case 'setRiskL':
        return {...state, app: Object.assign({}, state.app, {riskL: action.payload})};
      default:
        return state;
    }
  }
  
  function renderWithRedux(component, {initialState, store = createStore(reducer, initialState)} = {}){
    return {
      ...render(<Provider store={store}>{component}</Provider>)
    }
  } 
  it('renders with default riskL', () => {
    renderWithRedux(<RiskLevelSelector/>);
    expect(screen.getByTestId('risk-level').innerHTML).toContain(`10`);
  }); 
  
  // it('should render selected option', async () => {
  //   renderWithRedux(<RiskLevelSelector/>);

  //   // userEvent.selectOptions(screen.getByTestId("riskL-select-option"), ["6"]);
  //   // await waitFor(() => expect(screen.getByTestId('risk-level').innerHTML).toBe('Risk Level: 6'));    
  //   // expect(screen.getByTestId("4").selected).toBe(true);

  // });

  it('should show the alert message when the selected value is less then minimum', async () => {
    axiosMock.get.mockResolvedValueOnce({data: cones});
    renderWithRedux(<RiskLevelSelector/>);
    await userEvent.selectOptions(screen.getByTestId("riskL-select-option"), ["2"]);
    await waitFor(() => expect(screen.getByTestId('alert-msg').innerHTML).toBe('Sorry! Min Value is 3'));
  })
})
