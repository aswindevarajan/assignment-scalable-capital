import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import { calculateTimeSeries } from "./utils";
import { getCones } from "./services/cones.service";



const TableBody = (data) => {
  const { months, dataGood, dataMedian, dataBad } = data;
  let wrapper = months.map((val, index) => {
    return (
      <tr key={index}>
        <td>{val}</td>
        <td>{dataGood[index]}</td>
        <td>{dataMedian[index]}</td>
        <td>{dataBad[index]}</td>
      </tr>
    );
  });

  return wrapper;
};

TableBody.propTypes = {
  data: PropTypes.array,
}


const Table = () => {
  const [cones, setCones] = useState([]);
  let riskLevel = useSelector(state => state.app.riskL);
  let initialSum = useSelector(state => state.app.initialSum)
  let fee = useSelector(state => state.app.fee);
  let years = useSelector(state => state.app.years);

  useEffect(() => {
    getCones()
      .then((vida) => {
        setCones(vida);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  if (cones.length > 0) {
    const cone = cones.filter((cone) => cone.riskLevel == riskLevel)[0];
    
    let timeSeries = calculateTimeSeries({
      mu: cone.mu,
      sigma: cone.sigma,
      years,
      initialSum,
      monthlySum: 200,
      fee,
    });

    const months = timeSeries.median.map((v, idx) => idx);
    var dataGood = timeSeries.upper95.map((v) => v.y);
    let dataMedian = timeSeries.median.map((v) => v.y);
    const dataBad = timeSeries.lower05.map((v) => v.y);

    const viva = ["month", "good", "median", "bad"];
    return (
      <div data-testid="history-table">
      <table >
        <thead>
          <tr>
            {viva.map((val) => {
              return <th key={val}>{val}</th>;
            })}
          </tr>
        </thead>
        <tbody>{TableBody({ months, dataGood, dataMedian, dataBad })}</tbody>
      </table>
      </div>
    );
  } else {
    return <div data-testid="prompt-user">Please Select your risk level...</div>;
  }
};

export default Table;
