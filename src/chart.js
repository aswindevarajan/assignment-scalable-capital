import React, { useState, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { Chart as ChartJs } from "chart.js";
import { calculateTimeSeries } from "./utils";
import { getCones } from "./services/cones.service";

const Chart = () => {
  const [cones, setCones] = useState([]);
  let riskLevel = useSelector(state => state.app.riskL);
  let initialSum = useSelector(state => state.app.initialSum);
  let fee = useSelector(state => state.app.fee);
  let years = useSelector(state => state.app.years);
  
  const cRef = useRef(null);
  const [cInstance, setCInstance] = useState(null);

  let data = { datasets: [] };

  if (cones.length > 0) {
    const { mu, sigma } = cones.filter(
      (cone) => cone.riskLevel == riskLevel
    )[0];
    const monthlySum = 200;

    const timeSeries = calculateTimeSeries({
      mu,
      sigma,
      years,
      initialSum,
      fee,
      monthlySum,
    });

    const labels = timeSeries.median.map((v, idx) =>
      idx % 12 == 0 ? idx / 12 : ""
    );
    const dataMedian = timeSeries.median.map((v) => v.y);
    const dataGood = timeSeries.upper95.map((v) => v.y);
    const dataBad = timeSeries.lower05.map((v) => v.y);

    data = {
      datasets: [
        {
          data: dataGood,
          label: "Good performance",
          borderColor: "rgba(100, 255, 100, 0.2)",
          fill: false,
          pointRadius: 0,
        },
        {
          data: dataMedian,
          label: "Median performance",
          borderColor: "rgba(100, 100, 100, 0.2)",
          fill: false,
          pointRadius: 0,
        },
        {
          data: dataBad,
          label: "Bad performance",
          borderColor: "rgba(255, 100, 100, 0.2)",
          fill: false,
          pointRadius: 0,
        },
      ],
      labels,
    };
  }

  const options = {
    responsive: false,
    scales: {
      xAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Years",
          },
          gridLines: {
            drawOnChartArea: false,
          },
        },
      ],
      yAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: "Valuation (EUR)",
          },
        },
      ],
    },
  };

  const config = {
    type: "line",
    data,
    options,
  };

  useEffect(() => {
    if (cRef && cRef.current) {
      const newCInstance = new ChartJs(cRef.current, config);
      getCones()
        .then((vida) => {
          setCones(vida);
          setCInstance(newCInstance);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [cRef, riskLevel]);

  return (
    <div>
      <canvas ref={cRef} width={600} height={400} />
    </div>
  );
};
export default Chart;
