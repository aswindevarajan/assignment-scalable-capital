import React from 'react';
import { Provider } from 'react-redux'
import store from './store'
import MainPage from './mainPage';


const App = () => {
    return (
        <Provider store={store}>
            <MainPage></MainPage>
        </Provider>
    )
}

export default App;