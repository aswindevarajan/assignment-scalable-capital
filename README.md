## Step involved in the migration. 

### Steps

#### Why Did I move away from class based functions to functional based component?
Step 1: class to function
The very first step that I did was to convert all the existing class based components to functional components. 

Step 2: use of hooks
With the help of hooks I concentrated my feature logic at one place rather then scatter it around in different lifecycle methods of the component. 
This also helped me to avoid the hell of prop-drilling as they are simple, extenable, composible and flexible enought to share non-visual logic between components. 
the best use case in this application would be the use of ref to share the canvas between two libraries which directly interact with dom, react and chartjs.

All recent push in the react community is to move away from class based components and to adapt the functional components with extensive use of hooks to help to do so. The core reason is to reduce the tight-coupled code that comes with class at the frontend. 


Step 3: added redux store. 
Having a centralized store to share and update commonly shared parameter is a huge advantage when the complexity of application increase and the share of the non-visual code increases. redux was my ideal choice as I had used in all my previous react applications.

Step 4: use of opiniated redux/toolkit
Using a toolkit is something that I have recently come across, as it immensly offloads the write of "mutative" immutable update logic on state. This abstraction
brings down the common mistakes done while updating a state or with Object.assign while updating the state. 

Step 5: jest and react testing library
I have been able to write some test on each component created. except the chart component. 
- Unit Test: risk-level-selector.js
- Unit Test: table.js
- Unit Test: control-panel.js

Step 6: feature development of few more parameters for user to control 
Extending the redux store with parameters like fee, initial sum, years. 